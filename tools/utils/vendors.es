// array of modules that will built in separate bundle

export default [
    'classnames',
    'immutable',
    'lodash',
    'nskeymirror',
    'react',
    'react-dom',
    'react-redux',
    'react-router',
    'react-router-redux',
    'reselect',
    'redux',
];
