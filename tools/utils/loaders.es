export default [
    {
        test: /\.(js|jsx|es)$/,
        loader: 'babel',
    }, {
        test: /\.json$/,
        loader: 'json',
    }, {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader',
    }, {
        test: /\.(png|jpg)$/,
        loaders: [ 'url-loader?limit=150000' ],
    }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff',
    }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
    },
];
