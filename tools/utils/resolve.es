export default {
    extensions: [ '', '.jsx', '.js', '.es', '.json', '.scss', '.png' ],
    modulesDirectories: [ 'node_modules', 'src/shared', 'src' ],
};
