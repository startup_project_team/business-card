import path from 'path';

import { WEBPACK } from './config';

import vendors from './utils/vendors';
import loaders from './utils/loaders';
import resolve from './utils/resolve';
import commonPlugins from './plugins/common_plugins';
import productionPlugins from './plugins/prod_plugins';

export default {
    entry: {
        [WEBPACK.DEV_SERVER_APP_NAME]: [
            path.join(__dirname, '../src/main.es'),
        ],
        [WEBPACK.DEV_SERVER_VENDOR_NAME]: vendors,
    },

    output: {
        path: path.join(__dirname, '../build'),
        filename: '[name]-build.js',
    },

    module: {
        loaders,
    },

    resolve,

    plugins: commonPlugins.concat(productionPlugins),
};
