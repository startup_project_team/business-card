import Webpack from 'webpack';
import CLC from 'cli-color';

import WebpackProductionConfig from '../webpack.config.prod.es';

const compiler = Webpack(WebpackProductionConfig);

compiler.run((err) => {
    if (err) {
        console.error(CLC.red(err));
    } else {
        console.log(CLC.green('\nBusiness card build success.\n'));
    }
});
