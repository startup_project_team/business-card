import Webpack from 'webpack';
import path from 'path';

import ExtractTextWebpackPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import { WEBPACK } from '../config';

export default [
    new Webpack.optimize.CommonsChunkPlugin({
        name: [ WEBPACK.DEV_SERVER_VENDOR_NAME ],
        minChunks: Infinity,
    }),

    new ExtractTextWebpackPlugin('[name]-build.css'),

    new Webpack.NoErrorsPlugin(),

    new HtmlWebpackPlugin({
        title: 'WebileSoft',
        template: path.join(__dirname, '../../src/template/index.ejs'),
        favicon: path.join(__dirname, '../../src/shared/img/logo_white.png'),
    }),
];
