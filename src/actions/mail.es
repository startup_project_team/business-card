import superagent from 'superagent';

import {
    SPAM_MAIL,
} from 'constants/mail';

import {
    Actions as ToastActions,
    Constants as ToastConstants,
} from 'domains/toasts';

export function sendEmail (email, name, comment) {
    superagent
        .post(`https://formspree.io/${SPAM_MAIL}`)
        .send({
            message: `Response from our business card\nemail: ${email}\nname: ${name}\ncomment: ${comment}`,
        })
        .end((error) => {
            if (error) {
                console.error(error); // eslint-disable-line no-console
            } else {
                ToastActions.showToast(ToastConstants.SENT_MAIL_SUCCESS_MESSAGE);
            }
        });
}
