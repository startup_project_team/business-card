export function scrollToBottom (stepLength) {
    const height = document.body.scrollHeight;
    let progress = 0;

    const step = () => {
        progress = Math.min(progress + stepLength, height);
        window.scrollTo(0, progress);

        if (progress < height) {
            window.requestAnimationFrame(step);
        }
    };

    window.requestAnimationFrame(step);
}

export function scrollToTop () {
    window.scrollTo(0, 0);
}
