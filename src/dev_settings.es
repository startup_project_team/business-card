import _ from 'lodash';
import Immutable from 'immutable';
import store from 'store';
import installDevTools from 'immutable-devtools';

window._ = _;
window.Immutable = Immutable;
window.store = store;

installDevTools(Immutable);
