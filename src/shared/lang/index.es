import _ from 'lodash';
import React from 'react';

// dictionary
import {
    NonTranslated,
    Translated,
} from './dictionary';

import AbstractComponent from 'components/abstract_component';

function getTranslatedLabel (label, currentLanguage) {

    if (!currentLanguage) {
        throw new Error(`No language was passsed. ${currentLanguage}`);
    }

    const currentDictionary = Translated[currentLanguage];

    if (!currentDictionary) {
        throw new Error(`Dictionary for this language not found. ${currentLanguage}`);
    }

    let translatedLabel = currentDictionary[label];

    if (translatedLabel) {
        return translatedLabel;
    }

    translatedLabel = NonTranslated[label];

    if (translatedLabel) {
        // return `### ${translatedLabel} ###`;
        return translatedLabel;
    }

    throw new Error(`Incorrent label. Please add label: ${label} to dictionary.`);
}

/**
 * Return translated label
 *
 * @param {String} label                - label to translate
 * @param {String} currentLanguage      - iso code of current language as 'en-GB'
 *
 * @return {String}                     - trasnlated label with inserted keys
 */
export function translate (label, currentLanguage) {
    return getTranslatedLabel(label, currentLanguage);
}

/**
 * Return translated label with inserted alias
 *
 * @param {Object} label                - object with label to translate
 * @param {String} label.alias          - label to trasnlate
 * @param {Array} label.subs            - array of keys and values to insert
 * @param {String} label.subs[i].key    - key for evaluate place to replace ( 'abcd${some}efg', key shoud be some )
 * @param {String} label.subs[i].value  - value to insert
 * @param {String} currentLanguage      - iso code of current language as 'en-GB'
 *
 * @return {String}                     - trasnlated label with inserted keys
 */
export function translateFormattedLabel (label, currentLanguage) {

    if (!label || !label.alias) {
        return translate(label, currentLanguage);
    }

    let translatedLabel = getTranslatedLabel(label.alias, currentLanguage);

    _.forEach(label.subs, (sub) => {
        translatedLabel = translatedLabel.replace(`$\{${sub.key}}`, sub.value);
    });

    return translatedLabel;
}

/**
 * Special component to translate labels on component layer
 *
 * Only one param - label or alias should be passed as `children` or using props as `label`
 * You can provide class via props as className
 */
export class Translater extends AbstractComponent {
    render () {
        const label = this.props.label || this.props.children;

        return <span className={this.props.className}>{translateFormattedLabel(label, this.context.currentLanguage)}</span>;
    }
}

Translater.propTypes = {
    label: React.PropTypes.string,
};

export default {
    Translater,
    translate,
    translateFormattedLabel,
};
