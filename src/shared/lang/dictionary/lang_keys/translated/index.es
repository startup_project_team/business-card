// THERE SHOULD BE ORGANIZED IMPORT OF ALL LANGUAGES THAT OUR APP IS SUPPORT
import enUS from './en-us';
import ruRU from './ru-ru';

export default {
    'en-US': enUS,
    'ru-RU': ruRU,
};
