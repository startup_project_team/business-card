import Translated from './lang_keys/translated';
import NonTranslated from './lang_keys/non-translated';

// exporting of dictionary
export {
    NonTranslated,
    Translated,
};
