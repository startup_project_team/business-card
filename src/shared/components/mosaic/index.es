import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import AbstractComponent from 'components/abstract_component';
import './style';

class Mosaic extends AbstractComponent {
    render () {
        const ItemComponent = this.props.itemComponent;

        return (
            <ul className={classNames('mosaic-container', this.props.className)} >
                {_.map(this.props.items, (item, index) =>
                    <li
                        className="mosaic-item-container"
                        key={`${item.title}_${index}`}
                    >
                        <ItemComponent {...item} />
                    </li>
                )}
            </ul>
        );
    }
}

Mosaic.propTypes = {
    itemComponent: React.PropTypes.any.isRequired,  // eslint-disable-line
    items: React.PropTypes.array.isRequired,
    className: React.PropTypes.string,
};

export default Mosaic;
