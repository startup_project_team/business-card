import React from 'react';
import classNames from 'classnames';

import './style';

import AbstractComponent from 'components/abstract_component';
import { Translater } from 'lang';

export default class Toast extends AbstractComponent {
    render () {
        return (
            <div
                className={classNames('toast', {
                    'fade-out': this.props.fadeOut,
                })}
            >
                <i className="fa fa-check" />
                <Translater
                    className="toast-message"
                    label={this.props.label}
                />
            </div>
        );
    }
}

Toast.propTypes = {
    label: React.PropTypes.string.isRequired,
    fadeOut: React.PropTypes.bool,
};
