import { createSelector } from 'reselect';

// domains selectors
import { Selectors as ToastsSelectors } from 'domains/toasts';

export const toastsContainerSelector = createSelector(
    [ ToastsSelectors.toasts ],
    ( toasts ) => {
        return {
            toasts,
        };
    }
);
