import React from 'react';
import AbstractComponent from 'components/abstract_component';
import { connect } from 'react-redux';

import './style';

import { toastsContainerSelector } from './selectors';

import Toast from './toast';

class ToastsContainer extends AbstractComponent {
    render () {
        const toasts = [];

        this.props.toasts.forEach((toast, index) => toasts.push(
            <Toast
                label={toast.get('label')}
                fadeOut={toast.get('fadeOut')}
                key={`${toast}_${index}`}
            />
        ));

        return (
            <div className="toasts-container" >
                {toasts}
            </div>
        );
    }
}

ToastsContainer.propTypes = {
    toasts: React.PropTypes.object.isRequired,  // immutable list of strings
};

export default connect(toastsContainerSelector)(ToastsContainer);
