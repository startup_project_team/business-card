import { Paths } from 'router/constants';

export const MenuItems = [
    {
        id: 'development',
        key: 'KEY_HEADER_DEVELOPMENT',
        to: Paths.DEVELOPMENT,
        // submenu: [
        //     {
        //         id: 'service_web',
        //         key: 'KEY_HEADER_WEB_DEVELOPMENT',
        //         to: Paths.WEB,
        //     }, {
        //         id: 'service_mobile',
        //         key: 'KEY_HEADER_MOBILE_DEVELOPMENT',
        //         to: Paths.MOBILE,
        //     }, {
        //         id: 'service_big_data',
        //         key: 'KEY_HEADER_BIG_DATA_DEVELOPMENT',
        //         to: Paths.BIG_DATA,
        //     }, {
        //         id: 'service_machine_learning',
        //         key: 'KEY_HEADER_MACHINE_LEARNING_DEVELOPMENT',
        //         to: Paths.MACHINE_LEARNING,
        //     }, {
        //         id: 'service_cms',
        //         key: 'KEY_HEADER_CMS_DEVELOPMENT',
        //         to: Paths.CMS,
        //     }   // eslint-disable-line
        // ]   // eslint-disable-line
    }, {
        id: 'seo_header',
        key: 'KEY_HEADER_SEO',
        to: Paths.SEO,
    }, {
        id: 'portfolio',
        key: 'KEY_HEADER_PORTFOLIO',
        to: Paths.PORTFOLIO,
    }, {
        id: 'about_us',
        key: 'KEY_HEADER_ABOUT_US',
        to: Paths.ABOUT_US,
    }, {
        id: 'contact_with_us',
        key: 'KEY_HEADER_CONTACT_US',
        to: Paths.CONTACT_US,
    }   // eslint-disable-line
];

export const MenuItemsIndexes = {
    UNKNOWN: -1,
    DEVELOPMENT: 0,
    SEO: 1,
    PORTFOLIO: 2,
    ABOUT_US: 3,
    CONTACT_US: 4,
};
