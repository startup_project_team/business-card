import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { Link } from 'react-router';

import './style';

// shared
import AbstractComponent from 'components/abstract_component';
import { Translater } from 'lang';
import LanguagePicker from 'components/language_picker';

// selectors
import { headerSelector } from './selectors';

// constants
import { Paths } from 'router/constants';
import { MenuItems } from './constants';

class Header extends AbstractComponent {
    render () {

        const menuItems = [];

        MenuItems.forEach((menuItem, index) => {
            const submenuItems = [];

            if (!!menuItem.submenu) {
                menuItem.submenu.forEach((submenuItem) => {
                    submenuItems.push(
                        <li
                            className="navigation-sub-item"
                            key={submenuItem.id}
                        >
                            <Link to={submenuItem.to} >
                                <Translater
                                    className="navigation-submenu-item-text"
                                    label={submenuItem.key}
                                />
                            </Link>
                        </li>

                    );
                });
            }

            menuItems.push(
                <li
                    className={classNames('navigation-item', menuItem.cssMarker, {
                        'selected': index === this.props.selectedIndex,
                    })}
                    key={menuItem.id}
                >
                    <Link to={menuItem.to} >
                        <Translater
                            className="navigation-item-text"
                            label={menuItem.key}
                        />
                    </Link>
                    {!!menuItem.submenu &&
                        <div className="navigation-item-submenu" >
                            <ul className="navigation-submenu-list">
                                {submenuItems}
                            </ul>
                        </div>
                    }
                </li>
            );
        });

        return (
            <div className="header">
                <Link to={Paths.HOME} >
                    <div className="logo">
                        <Translater className="logo-text">{'KEY_HEADER_LOGO_TITLE_1'}</Translater>
                        <div className="logo-image" />
                        <Translater className="logo-text">{'KEY_HEADER_LOGO_TITLE_2'}</Translater>
                    </div>
                </Link>
                <ul className="navigation">
                    {menuItems}
                    <LanguagePicker
                        isEnglishLanguage={this.props.isEnglishLanguage}
                        isRussianLanguage={this.props.isRussianLanguage}
                    />
                </ul>
            </div>
        );
    }
}

Header.propTypes = {
    isEnglishLanguage: React.PropTypes.bool.isRequired,
    isRussianLanguage: React.PropTypes.bool.isRequired,
    selectedIndex: React.PropTypes.number.isRequired,
};

export default connect(headerSelector)(Header);
