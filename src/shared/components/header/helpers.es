// constants
import { Paths } from 'router/constants';
import { MenuItemsIndexes } from './constants';

export function getSelectedIndex (pathname) {
    if (pathname === Paths.DEVELOPMENT ||
        pathname === Paths.WEB ||
        pathname === Paths.MOBILE ||
        pathname === Paths.CMS ||
        pathname === Paths.BIG_DATA ||
        pathname === Paths.MACHINE_LEARNING) {
        return MenuItemsIndexes.DEVELOPMENT;
    } else if (pathname === Paths.SEO) {
        return MenuItemsIndexes.SEO;
    } else if (pathname === Paths.PORTFOLIO) {
        return MenuItemsIndexes.PORTFOLIO;
    } else if (pathname === Paths.ABOUT_US) {
        return MenuItemsIndexes.ABOUT_US;
    } else if (pathname === Paths.CONTACT_US) {
        return MenuItemsIndexes.CONTACT_US;
    }

    return MenuItemsIndexes.UNKNOWN;
}
