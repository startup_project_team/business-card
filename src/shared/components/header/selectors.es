import { createSelector } from 'reselect';

// domains selectors
import { pathname } from 'router/selectors';
import { Selectors as SystemSelectors } from 'domains/system';

// helpers
import { getSelectedIndex } from './helpers';

export const headerSelector = createSelector(
    [ pathname, SystemSelectors.isRussianLanguage, SystemSelectors.isEnglishLanguage ],
    ( path, isRussianLanguage, isEnglishLanguage ) => {
        return {
            selectedIndex: getSelectedIndex(path),
            isRussianLanguage,
            isEnglishLanguage,
        };
    }
);
