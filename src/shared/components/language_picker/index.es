import React from 'react';
import classNames from 'classnames';

// common
import { Translater } from 'lang';
import AbstractComponent from 'components/abstract_component';

import './style';

// actions
import * as Actions from './actions';

export default class LanguagePicker extends AbstractComponent {
    render () {
        const ruLanguageItemClassName = classNames('language-item', {
            'selected-language': this.props.isRussianLanguage,
        });

        const enLanguageItemClassName = classNames('language-item', {
            'selected-language': this.props.isEnglishLanguage,
        });

        return (
            <div className="language-picker">
                <span
                    className={ruLanguageItemClassName}
                    onClick={Actions.onRULanguagePressed}
                >
                    <Translater>{'KEY_FOOTER_LANGUAGE_RU'}</Translater>
                </span>
                <div className="divider" />
                <span
                    className={enLanguageItemClassName}
                    onClick={Actions.onENLanguagePressed}
                >
                    <Translater>{'KEY_FOOTER_LANGUAGE_EN'}</Translater>
                </span>
            </div>
        );
    }
}

LanguagePicker.propTypes = {
    isEnglishLanguage: React.PropTypes.bool.isRequired,
    isRussianLanguage: React.PropTypes.bool.isRequired,
};
