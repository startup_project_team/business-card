import {
    Actions as SystemActions,
    Constants as SystemConstants,
} from 'domains/system';

export function onENLanguagePressed () {
    SystemActions.updateCurrentLanguage(SystemConstants.AvailableLanguages.ENGLISH);
}

export function onRULanguagePressed () {
    SystemActions.updateCurrentLanguage(SystemConstants.AvailableLanguages.RUSSIAN);
}
