import React from 'react';
import { Translater, translate } from 'lang';

import './style';

// common
import AbstractComponent from 'components/abstract_component';
import Button from 'components/button';

// actions
import * as EmailActions from 'actions/mail';

export default class MiniContactUs extends AbstractComponent {
    render () {
        return (
            <div className="minicontact-us">
                <div className="minicontact-us-container">
                    <Translater
                        className="minicontact-us-title"
                        label={this.props.mainTitle}
                    />
                    {
                        this.props.showSubtilte &&
                        <Translater
                            className="minicontact-us-subtitle"
                            label="KEY_MINI_CONTACT_US_SUBTITLE"
                        />
                    }
                    <input
                        ref={(input) => this.emailInput = input} // eslint-disable-line no-return-assign
                        className="minicontact-us-email input"
                        placeholder={translate('KEY_MINI_CONTACT_US_EMAIL_INPUT', this.context.currentLanguage)}
                    />
                    <input
                        ref={(input) => this.nameInput = input} // eslint-disable-line no-return-assign
                        className="minicontact-us-name input"
                        placeholder={translate('KEY_MINI_CONTACT_US_NAME_INPUT', this.context.currentLanguage)}
                    />
                    <textarea
                        ref={(textarea) => this.commentArea = textarea} // eslint-disable-line no-return-assign
                        rows="9"
                        className="minicontact-us textarea"
                        placeholder={translate(this.props.textareaPlaceholder, this.context.currentLanguage)}
                    />
                    <Button
                        className="minicontact-us-button"
                        label="KEY_MINI_CONTACT_US_SEND_BUTTON"
                        onClick={() => {
                            EmailActions.sendEmail(
                                this.emailInput.value,
                                this.nameInput.value,
                                this.commentArea.value
                            );
                        }}
                    />
                </div>
            </div>
        );
    }
}

MiniContactUs.propTypes = {
    mainTitle: React.PropTypes.string.isRequired,
    showSubtilte: React.PropTypes.bool,
    textareaPlaceholder: React.PropTypes.string,
};

MiniContactUs.defaultProps = {
    showSubtilte: true,
    textareaPlaceholder: 'KEY_MINI_CONTACT_US_PROBLEM_INPUT',
};
