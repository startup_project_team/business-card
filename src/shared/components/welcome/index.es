import React from 'react';
import AbstractComponent from 'components/abstract_component';
import classNames from 'classnames';

import './style';

import { Translater } from 'lang';

export default class Welcome extends AbstractComponent {
    render () {
        return (
            <div className={classNames('welcome-container', this.props.className)}>
                <div className={this.props.imageClass} />
                <Translater
                    label={this.props.title}
                    className="welcome-title"
                />
                <Translater
                    label={this.props.subtitle}
                    className="welcome-subtitle"
                />
            </div>
        );
    }
}

Welcome.propTypes = {
    imageClass: React.PropTypes.string.isRequired,
    subtitle: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
    className: React.PropTypes.string,
};
