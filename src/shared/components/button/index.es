import React from 'react';
import classNames from 'classnames';

// common
import { Translater } from 'lang';
import AbstractComponent from 'components/abstract_component';

import './style';

export default class Button extends AbstractComponent {
    render () {
        const buttonClass = classNames('shared-button', this.props.className);

        return (
            <button
                className={buttonClass}
                onClick={this.props.onClick}
            >
                <Translater
                    className="shared-button-label"
                    label={this.props.label}
                />
            </button>
        );
    }
}

Button.propTypes = {
    label: React.PropTypes.string.isRequired,
    onClick: React.PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
    className: React.PropTypes.string,
};
