import React from 'react';
import { Link } from 'react-router';

// common
import { Translater, translate } from 'lang';
import AbstractComponent from 'components/abstract_component';

import './style';

// constants
import { Paths } from 'router/constants';

export default class Footer extends AbstractComponent {
                        // <Link
                        //     to={Paths.WEB}
                        // >
                        //     <Translater
                        //         className="footer-main-title"
                        //         label="KEY_FOOTER_WEB_DEVELOPMENT"
                        //     />
                        // </Link>
                        // <Link
                        //     to={Paths.MOBILE}
                        // >
                        //     <Translater
                        //         className="footer-main-title"
                        //         label="KEY_FOOTER_MOBILE_DEVELOPMENT"
                        //     />
                        // </Link>
                        // <Link
                        //     to={Paths.MACHINE_LEARNING}
                        // >
                        //     <Translater
                        //         className="footer-main-title"
                        //         label="KEY_FOOTER_MACHINE_LEARNING"
                        //     />
                        // </Link>

    render () {
        return (
            <div className="footer-container">
                <div className="footer">
                    <div className="footer-about-us block">
                        <Link
                            to={Paths.ABOUT_US}
                        >
                            <Translater
                                className="footer-main-title"
                                label="KEY_FOOTER_ABOUT_US"
                            />
                        </Link>
                        <Link
                            to={Paths.ABOUT_US}
                        >
                            <Translater
                                className="footer-small-title"
                                label="KEY_FOOTER_OUR_TEAM"
                            />
                        </Link>
                        <Link
                            to={Paths.CONTACT_US}
                        >
                            <Translater
                                className="footer-small-title"
                                label="KEY_FOOTER_OUR_LOCATION"
                            />
                        </Link>
                        <Link
                            to={Paths.ABOUT_US}
                        >
                            <Translater
                                className="footer-small-title"
                                label="KEY_FOOTER_REVIEWS"
                            />
                        </Link>
                        <Link
                            to={Paths.ABOUT_US}
                        >
                            <Translater
                                className="footer-small-title"
                                label="KEY_FOOTER_VACANCIES"
                            />
                        </Link>
                    </div>
                    <div className="footer-services block">
                        <Link
                            to={Paths.SEO}
                        >
                            <Translater
                                className="footer-main-title"
                                label="KEY_FOOTER_SEO"
                            />
                        </Link>
                        <Link
                            to={Paths.DEVELOPMENT}
                        >
                            <Translater
                                className="footer-main-title"
                                label="KEY_FOOTER_DEVELOPMENT"
                            />
                        </Link>
                    </div>
                    <div className="footer-spy block">
                        <div className="social">
                            <a
                                className="linkedin"
                                href="https://www.linkedin.com/webilesoft"
                            >
                                <i className="fa fa-linkedin fa-lg" />
                            </a>
                            <a
                                className="vkontakte"
                                href="https://www.vk.com/webilesoft"
                            >
                                <i className="fa fa-vk fa-lg" />
                            </a>
                            <a
                                className="facebook"
                                href="https://www.facebook.com/profile.php?id=100014731809242"
                            >
                                <i className="fa fa-facebook fa-lg" />
                            </a>
                        </div>
                        <Translater className="support-email">
                            {'KEY_FOOTER_SUPPORT_EMAIL'}
                        </Translater>
                        <a
                            href={`tel:${translate('KEY_FOOTER_SUPPORT_PHONE', this.context.currentLanguage)}`}
                            className="support-phone"
                        >
                            <Translater
                                label="KEY_FOOTER_SUPPORT_PHONE"
                                className="support-phone-text"
                            />
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}
