import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

// styles
import './style';

// shared
import AbstractComponent from 'components/abstract_component';
import { Translater } from 'lang';

export default class ServicesItem extends AbstractComponent {
    render () {

        let serviceItemImage = <div className={classNames('service-item-image', this.props.imageClass)} />;

        if (!!this.props.path) {
            serviceItemImage = <Link to={this.props.path} >
                {serviceItemImage}
            </Link>;
        }

        return (
            <div className="services-item">
                {serviceItemImage}
                <div className="service-item-text-container">
                    <Translater
                        className="service-item-title"
                        label={this.props.title}
                    />
                    <Translater
                        className="service-item-label"
                        label={this.props.label}
                    />
                </div>
            </div>
        );
    }
}

ServicesItem.propTypes = {
    imageClass: React.PropTypes.string.isRequired,
    label: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
    path: React.PropTypes.string,
};
