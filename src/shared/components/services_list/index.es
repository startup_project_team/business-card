import React from 'react';
import './style';
import _ from 'lodash';
import classNames from 'classnames';

import AbstractComponent from 'components/abstract_component';
import ServiceItem from './service_item';

export default class ServicesList extends AbstractComponent {
    render () {
        return (
            <div className={classNames('services-list', this.props.className)} >
                {
                    _.map(this.props.items, (item, index) =>
                        <ServiceItem
                            key={`${item.title}_${index}`}
                            imageClass={item.imageClass}
                            title={item.title}
                            label={item.label}
                            path={item.path}
                        />)
                }
            </div>
        );
    }
}

ServicesList.propTypes = {
    items: React.PropTypes.array.isRequired,
    className: React.PropTypes.string,
};
