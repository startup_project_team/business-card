export default [
    {
        description: 'KEY_LOREM_IPSUM',
        link: 'http://hotel-buta.by/en/',
        picture: 'boutique',
        title: 'KEY_MOCK_BOUTIQUE',
        technologies: [
            'KEY_TECHNOLOGY_JS',
            'KEY_TECHNOLOGY_CSS3',
            'KEY_TECHNOLOGY_HTML5',
        ],
    }, {
        description: 'KEY_LOREM_IPSUM',
        link: 'http://neworleans.pl/',
        picture: 'new-orleans',
        title: 'KEY_MOCK_EROTICA',
        technologies: [
            'KEY_TECHNOLOGY_JS',
            'KEY_TECHNOLOGY_CSS3',
            'KEY_TECHNOLOGY_HTML5',
        ],
    }, {
        description: 'KEY_LOREM_IPSUM',
        link: 'https://www.casinoeuro.com/',
        picture: 'casino-euro',
        title: 'KEY_MOCK_CASINO',
        technologies: [
            'KEY_TECHNOLOGY_JS',
            'KEY_TECHNOLOGY_CSS3',
            'KEY_TECHNOLOGY_HTML5',
        ],
    },
    {
        description: 'KEY_LOREM_IPSUM',
        link: 'http://myconstructor.ru/',
        picture: 'myconstructor',
        title: 'KEY_MOCK_MYCONSTRUCTOR',
        technologies: [
            'KEY_TECHNOLOGY_CANVAS',
            'KEY_TECHNOLOGY_JS',
            'KEY_TECHNOLOGY_CSS3',
            'KEY_TECHNOLOGY_HTML5',
        ],
    }, {
        description: 'KEY_LOREM_IPSUM',
        link: 'https://dev.by/',
        picture: 'dev',
        title: 'KEY_MOCK_DEV_BY',
        technologies: [
            'KEY_TECHNOLOGY_JS',
            'KEY_TECHNOLOGY_CSS3',
            'KEY_TECHNOLOGY_HTML5',
        ],
    }, {
        description: 'KEY_LOREM_IPSUM',
        link: 'http://mtr.by/',
        picture: 'metr',
        title: 'KEY_MOCK_METR',
        technologies: [
            'KEY_TECHNOLOGY_JS',
            'KEY_TECHNOLOGY_CSS3',
            'KEY_TECHNOLOGY_HTML5',
        ],
    },
];
