import React from 'react';
import ReactDOM from 'react-dom';

// root component
import Root from 'components/root';

// store
import store from 'store';

//history
import history from 'router/history';

// init actions
import { Actions as SystemActions } from 'domains/system';

window.onload = () => {
    SystemActions.initialize();

    ReactDOM.render(
        <Root
            store={store}
            history={history}
        />,
        document.getElementById('app-container')
    );
};
