import { createStore } from 'redux';

// reducers
import Reducers from 'reducers';

const store = createStore(
    Reducers
);

export default store;
