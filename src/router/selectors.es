export function selectLocationState (state) {
    return state.get('routing').toJS();
}

export function pathname (state) {
    const pathName = state.getIn([ 'routing', 'locationBeforeTransitions', 'pathname' ]);

    return pathName[0] === '/' ? pathName.substring(1, pathName.length) : pathName;
}
