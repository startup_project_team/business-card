import React from 'react';
import { Route, IndexRedirect } from 'react-router';

// Pathes
import { Paths } from 'router/constants';

// COMPONENTS

// general app component
import App from 'components/app';

// views
import AboutUs from 'components/about_us';
import ContactUs from 'components/contact_us';
import Portfolio from 'components/portfolio';
import Home from 'components/home';

import Development from 'components/development';
import SEO from 'components/service/seo';
// import WebDevelopment from 'components/service/web';
// import MobileDevelopment from 'components/service/mobile';
// import BigDataDevelopment from 'components/service/big-data';
// import MachineLearning from 'components/service/machine-learning';
// import CMSDevelopment from 'components/service/cms';

// actions
import { scrollToTop } from 'actions/scrolling';

export default () =>
    <Route
        path="/"
        component={App}
    >

        <IndexRedirect to={Paths.HOME} />

        <Route
            path={Paths.HOME}
            component={Home}
            onEnter={scrollToTop}
        />
        <Route
            path={Paths.ABOUT_US}
            component={AboutUs}
            onEnter={scrollToTop}
        />
        <Route
            path={Paths.CONTACT_US}
            component={ContactUs}
            onEnter={scrollToTop}
        />
        <Route
            path={Paths.PORTFOLIO}
            component={Portfolio}
            onEnter={scrollToTop}
        />

        <Route
            path={Paths.DEVELOPMENT}
            component={Development}
            onEnter={scrollToTop}
        />

        <Route
            path={Paths.SEO}
            component={SEO}
            onEnter={scrollToTop}
        />
    </Route>;
        // <Route
        //     path={Paths.WEB}
        //     component={WebDevelopment}
        // />
        // <Route
        //     path={Paths.MOBILE}
        //     component={MobileDevelopment}
        // />
        // <Route
        //     path={Paths.BIG_DATA}
        //     component={BigDataDevelopment}
        // />
        // <Route
        //     path={Paths.MACHINE_LEARNING}
        //     component={MachineLearning}
        // />
        // <Route
        //     path={Paths.CMS}
        //     component={CMSDevelopment}
        // />
