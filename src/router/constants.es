export const Paths = {
    ABOUT_US: 'about-us',
    CONTACT_US: 'contact-us',
    HOME: 'home',
    PORTFOLIO: 'portfolio',

    DEVELOPMENT: 'development',
    SEO: 'seo',
    WEB: 'web-development',
    MOBILE: 'mobile-development',
    BIG_DATA: 'big-data',
    MACHINE_LEARNING: 'machine-learning',
    CMS: 'cms',
};
