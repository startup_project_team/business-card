import { Paths } from 'router/constants';
import history from 'router/history';

export function goToHome () {
    history.push(Paths.HOME);
}

export function goToAboutUs () {
    history.push(Paths.ABOUT_US);
}

export function goToPortfolio () {
    history.push(Paths.PORTFOLIO);
}

export function goToServiceSEO () {
    history.push(Paths.SEO);
}

export function goToServiceWeb () {
    history.push(Paths.WEB);
}

export function goToServiceMobile () {
    history.push(Paths.MOBILE);
}

export function goToServiceBigData () {
    history.push(Paths.BIG_DATA);
}

export function goToServiceMachineLearning () {
    history.push(Paths.MACHINE_LEARNING);
}

export function goToServiceCms () {
    history.push(Paths.CMS);
}
