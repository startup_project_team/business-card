import { combineReducers } from 'redux-immutable';

// reducers
import routingReducer from 'router/reducer';

// domains reducers
import systemReducer from 'domains/system/reducer';
import toastsReducer from 'domains/toasts/reducer';

export default combineReducers({
    routing: routingReducer,
    system: systemReducer,
    toasts: toastsReducer,
});
