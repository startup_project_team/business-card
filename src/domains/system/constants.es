export const AvailableLanguages = {
    RUSSIAN: 'ru-RU',
    ENGLISH: 'en-US',
};

// you can add country codes via iso3166
// list of avalable countries: http://www.geoplugin.com/iso3166
export const RussianSpeakingCountries = [
    'BY',
    'RU',
    'KZ',
    'UA',
];
