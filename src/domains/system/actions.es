// store
import store from 'store';

// constants
import ActionTypes from './action_types';

// helpers
import * as Helpers from './helpers';

// constants
import * as Constants from './constants';

export function updateCurrentLanguage (language) {
    store.dispatch({
        type: ActionTypes.UPDATE_CURRENT_LANGUAGE,
        payload: language,
    });
}

export function initialize () {
    let countryCode = window.navigator.language;

    countryCode = Helpers.languageIsSupported(countryCode) ? countryCode : Constants.AvailableLanguages.ENGLISH;

    if (typeof geoplugin_countryCode === 'function') {    // eslint-disable-line
        countryCode = geoplugin_countryCode();    // eslint-disable-line
        // russian is default language
        if (!Helpers.isRussianSpeakingCountry(countryCode)) {
            updateCurrentLanguage(Constants.AvailableLanguages.ENGLISH);
        }
    } else {
        updateCurrentLanguage(countryCode);
    }
}
