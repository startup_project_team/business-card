import { AvailableLanguages } from './constants';

export const currentLanguage = (state) => state.getIn([ 'system', 'currentLanguage' ]);

export const isRussianLanguage = (state) => state.getIn([ 'system', 'currentLanguage' ]) === AvailableLanguages.RUSSIAN;
export const isEnglishLanguage = (state) => state.getIn([ 'system', 'currentLanguage' ]) === AvailableLanguages.ENGLISH;
