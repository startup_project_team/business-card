import _ from 'lodash';
import {
    RussianSpeakingCountries,
    AvailableLanguages,
} from './constants';

export function isRussianSpeakingCountry (countryCode) {
    return _.includes(RussianSpeakingCountries, countryCode);
}

export function languageIsSupported (lang) {
    return _.includes(AvailableLanguages, lang);
}
