import keyMirror from 'nskeymirror';

export default keyMirror({
    SHOW_TOAST: null,
    HIDE_TOAST: null,
    FADE_OUT_TOAST: null,
}, 'TOASTS_DOMAIN_ACTION_TYPE');
