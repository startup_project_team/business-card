import store from 'store';

import ActionTypes from './action_types';
import {
    TOAST_SHOW_TIMEOUT,
    TOAST_FADE_OUT_TIMEOUT,
} from './constants';

let hideToastTimeout;

export function showToast (label) {
    store.dispatch({
        type: ActionTypes.SHOW_TOAST,
        payload: label,
    });

    hideToastTimeout = setTimeout(fadeOutToast, TOAST_SHOW_TIMEOUT);
}

export function fadeOutToast () {
    clearTimeout(hideToastTimeout);
    hideToastTimeout = null;

    store.dispatch({
        type: ActionTypes.FADE_OUT_TOAST,
    });

    hideToastTimeout = setTimeout(hideToast, TOAST_FADE_OUT_TIMEOUT);
}

export function hideToast () {
    clearTimeout(hideToastTimeout);
    hideToastTimeout = null;

    store.dispatch({
        type: ActionTypes.HIDE_TOAST,
    });
}
