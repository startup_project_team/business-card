import Immutable from 'immutable';

// constants
import ActionTypes from './action_types';
import {
    ZERO,
} from './constants';

const initialState = Immutable.fromJS({
    list: [],
});

export default function toastsReducer (state = initialState, action) {
    switch (action.type) {
        case ActionTypes.SHOW_TOAST:
            return state.set('list', state.get('list').push(Immutable.fromJS({
                label: action.payload,
            })));

        case ActionTypes.FADE_OUT_TOAST:
            return state.setIn([ 'list', 0 ], state.getIn([ 'list', 0 ]).set('fadeOut', true));

        case ActionTypes.HIDE_TOAST:
            return state.set('list', state.get('list').remove(ZERO));

        default:
            return state;

    }
}
