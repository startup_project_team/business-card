import React from 'react';
import { connect } from 'react-redux';

// selectors
import { appSelector } from './selectors';

// common components
import Footer from 'components/footer';
import Header from 'components/header';
import ToastsContainer from 'components/toasts_container';
import './style';

class App extends React.Component {
    getChildContext () {
        return {
            currentLanguage: this.props.currentLanguage,
        };
    }

    render () {
        return (
            <div className="app">
                <ToastsContainer />
                <Header />
                <div className="app-content">
                    { this.props.children }
                    <Footer />
                </div>
            </div>
        );
    }
}

App.childContextTypes = {
    currentLanguage: React.PropTypes.string.isRequired,
};

export default connect(appSelector)(App);
