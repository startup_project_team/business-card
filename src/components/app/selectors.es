import { createSelector } from 'reselect';

// domains selectors
import { Selectors as SystemSelectors } from 'domains/system';

export const appSelector = createSelector(
    [ SystemSelectors.currentLanguage ],
    (currentLanguage) => {
        return {
            currentLanguage,
        };
    }
);
