import React from 'react';
import AbstractComponent from 'components/abstract_component';

export default class Mobile extends AbstractComponent {
    render () {
        return (
            <h1>{'Mobile'}</h1>
        );
    }
}
