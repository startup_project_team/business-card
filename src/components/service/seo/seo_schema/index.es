import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

import DevelopmentStep from 'components/development/development_schema/development_step';

export default class SeoSchema extends AbstractComponent {
    render () {
        return (
            <div className="seo-schema">
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_1"
                    title="KEY_SEO_SCHEMA_REQUEST_TITLE"
                    imageClass="request"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_2"
                    title="KEY_SEO_SCHEMA_ANALYSIS_TITLE"
                    imageClass="analysis"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_3"
                    title="KEY_SEO_SCHEMA_KEYWORDS_TITLE"
                    imageClass="keywords"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_4"
                    title="KEY_SEO_SCHEMA_CONTRACT_TITLE"
                    imageClass="contract"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_5"
                    title="KEY_SEO_SCHEMA_COMPANY_TITLE"
                    imageClass="company"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_6"
                    title="KEY_SEO_SCHEMA_ACCOUNT_TITLE"
                    imageClass="account"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_7"
                    title="KEY_SEO_SCHEMA_CORRECT_TITLE"
                    imageClass="correct"
                />
            </div>
        );
    }
}
