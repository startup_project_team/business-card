import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

import { SEO_SERVICES } from './constants';

// components
import { Translater } from 'lang';
import ServicesList from 'components/services_list';

export default class SeoServices extends AbstractComponent {
    render () {
        return (
            <div className="seo-services">
                <Translater
                    className="seo-services-title"
                    label="KEY_SEO_SERVICES_TITLE"
                />
                <ServicesList
                    items={SEO_SERVICES}
                    className="seo-services-list"
                />
            </div>
        );
    }
}
