export const SEO_SERVICES = [
    // {
    //     imageClass: 'search',
    //     title: 'KEY_SEO_ORGANIC_SEARCH_TITLE',
    //     label: 'KEY_SEO_ORGANIC_SEARCH_LABEL',
    // }, {
    //     imageClass: 'on-page-seo',
    //     title: 'KEY_SEO_ON_PAGE_SEO_TITLE',
    //     label: 'KEY_SEO_ON_PAGE_SEO_LABEL',
    // }, {
    //     imageClass: 'keyword-research',
    //     title: 'KEY_SEO_KEYWORD_RESEARCH_TITLE',
    //     label: 'KEY_SEO_KEYWORD_RESEARCH_LABEL',
    // },
    {
        imageClass: 'target-audience',
        title: 'KEY_SEO_TARGET_AUDIENCE_TITLE',
        label: 'KEY_SEO_TARGET_AUDIENCE_LABEL',
    }, {
        imageClass: 'region-settings',
        title: 'KEY_SEO_REGION_SETTINGS_TITLE',
        label: 'KEY_SEO_REGION_SETTINGS_LABEL',
    }, {
        imageClass: 'click-payment',
        title: 'KEY_SEO_CLICK_PAYMENT_TITLE',
        label: 'KEY_SEO_CLICK_PAYMENT_LABEL',
    }, {
        imageClass: 'flex-click-cost',
        title: 'KEY_SEO_FLEX_CLICK_COST_TITLE',
        label: 'KEY_SEO_FLEX_CLICK_COST_LABEL',
    },
];
