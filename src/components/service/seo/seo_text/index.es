import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

// components
import { Translater } from 'lang';

export default class SeoMainText extends AbstractComponent {
    render () {
        return (
            <div className="seo-text-container">
                <Translater
                    className="seo-main-text seo-text-first-part"
                    label="KEY_SEO_MAIN_TEXT_FIRST_PART"
                />
                <Translater
                    className="seo-main-text seo-text-second-part"
                    label="KEY_SEO_MAIN_TEXT_SECOND_PART"
                />
            </div>
        );
    }
}
