import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

// components
import MiniContactUs from 'components/mini_contact_us';
import { Translater } from 'lang';
import Welcome from 'components/welcome';
import SeoServices from './seo_services';
// import SeoMainText from './seo_text';
import SeoSchema from './seo_schema';

export default class SEO extends AbstractComponent {
    render () {
                // <SeoMainText />
        return (
            <div className="seo-container">
                <Welcome
                    imageClass="seo-image"
                    subtitle="KEY_SEO_WELCOME_SUBTITLE"
                    title="KEY_SEO_WELCOME_TITLE"
                />
                <SeoServices />
                <Translater
                    className="seo-schema-title"
                    label="KEY_SEO_SCHEMA_TITLE"
                />
                <SeoSchema />
                <MiniContactUs
                    mainTitle="KEY_SEO_TRY"
                    showSubtilte={false}
                    textareaPlaceholder="KEY_SEO_ABOUT_YOUR_COMPANY"
                />
            </div>
        );
    }
}
