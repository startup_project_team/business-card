import React from 'react';
import AbstractComponent from 'components/abstract_component';

export default class WebDevelopment extends AbstractComponent {
    render () {
        return (
            <h1>{'WebDevelopment'}</h1>
        );
    }
}
