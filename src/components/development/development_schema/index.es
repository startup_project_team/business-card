import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

import DevelopmentStep from './development_step';

export default class DevelopmentSchema extends AbstractComponent {
    render () {
        return (
            <div className="development-schema">
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_1"
                    title="KEY_DEVELOPMENT_SCHEMA_BUSINESS_REQ_TITLE"
                    label="KEY_DEVELOPMENT_SCHEMA_BUSINESS_REQ_DESCRIPTION"
                    imageClass="business-requirements"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_2"
                    title="KEY_DEVELOPMENT_SCHEMA_CONTRACT_TITLE"
                    label="KEY_DEVELOPMENT_SCHEMA_CONTRACT_DESCRIPTION"
                    imageClass="contract"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_3"
                    title="KEY_DEVELOPMENT_SCHEMA_SPECIFICATION_TITLE"
                    label="KEY_DEVELOPMENT_SCHEMA_SPECIFICATION_DESCRIPTION"
                    imageClass="specification"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_4"
                    title="KEY_DEVELOPMENT_SCHEMA_DESIGN_TITLE"
                    label="KEY_DEVELOPMENT_SCHEMA_DESIGN_DESCRIPTION"
                    imageClass="design"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_5"
                    title="KEY_DEVELOPMENT_SCHEMA_DEV_TITLE"
                    label="KEY_DEVELOPMENT_SCHEMA_DEV_DESCRIPTION"
                    imageClass="developemnt-testing"
                />
                <DevelopmentStep
                    step="KEY_DEVELOPMENT_SCHEMA_STEP_6"
                    title="KEY_DEVELOPMENT_SCHEMA_DEMO_TITLE"
                    label="KEY_DEVELOPMENT_SCHEMA_DEMO_DESCRIPTION"
                    imageClass="demo-support"
                />
            </div>
        );
    }
}
