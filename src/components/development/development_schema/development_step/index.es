import React from 'react';
import classNames from 'classnames';
import AbstractComponent from 'components/abstract_component';

import './style';

import { Translater } from 'lang';

export default class DevelopmentStep extends AbstractComponent {
    render () {
        return (
            <div className="development-step">
                {!!this.props.label ?
                    <Translater
                        className="development-step-title"
                        label={this.props.title}
                    /> : null}
                <div className="development-step-info" >
                    <Translater
                        className="development-step-number"
                        label={this.props.step}
                    />
                    <Translater
                        className="development-step-label"
                        label={!!this.props.label ? this.props.label : this.props.title}
                    />
                    <div className={classNames('development-step-image', this.props.imageClass)} />
                </div>
            </div>
        );
    }
}

DevelopmentStep.propTypes = {
    imageClass: React.PropTypes.string.isRequired,
    step: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
    label: React.PropTypes.string,
};
