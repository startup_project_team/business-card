import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

// constants
import { ANIMATION_STEP } from 'constants/scrolling';

// components
import MiniContactUs from 'components/mini_contact_us';
import Welcome from 'components/welcome';
import { Translater } from 'lang';
import Button from 'components/button';
import DevelopmentSchema from './development_schema';

// actions
import { scrollToBottom } from 'actions/scrolling';

export default class Development extends AbstractComponent {
    render () {
        return (
            <div className="development-container">
                <Welcome
                    imageClass="development-image"
                    subtitle="KEY_DEVELOPMENT_WELCOME_SUBTITLE"
                    title="KEY_DEVELOPMENT_WELCOME_TITLE"
                />
                <div className="development-main-section">
                    <Translater
                        className="development-title"
                        label="KEY_DEVELOPMENT_WEB_TITLE"
                    />
                    <Translater
                        className="development-description"
                        label="KEY_DEVELOPMENT_WEB_DESCRIPTION"
                    />
                    <Button
                        label="KEY_DEVELOPMENT_CONTACT_BUTTON"
                        onClick={() => scrollToBottom(ANIMATION_STEP)}
                        className="development-button"
                    />
                    <Translater
                        className="development-schema-title"
                        label="KEY_DEVELOPMENT_WEB_SCHEMA"
                    />
                    <Translater
                        className="development-description"
                        label="KEY_DEVELOPMENT_WEB_SCHEMA_DESCRIPTION_PART_1"
                    />
                    <Translater
                        className="development-description"
                        label="KEY_DEVELOPMENT_WEB_SCHEMA_DESCRIPTION_PART_2"
                    />
                    <DevelopmentSchema />
                </div>
                <MiniContactUs
                    mainTitle="KEY_DEVELOPMENT_CONTACT_TITLE"
                    showSubtilte={false}
                    textareaPlaceholder="KEY_DEVELOPMENT_PLACEHOLDER"
                />
            </div>
        );
    }
}
