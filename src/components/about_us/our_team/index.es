import React from 'react';
import './style';

// components
import AbstractComponent from 'components/abstract_component';
import TeamMember from './team_member';
import { Translater } from 'lang';

export default class OurTeam extends AbstractComponent {
    render () {
        return (
            <div className="our-team-container">
                <Translater
                    className="our-team-title"
                    label="KEY_OUR_TEAM_TITLE"
                />
                <ul className="our-team-list">
                    <TeamMember
                        email="edvards_kazlouski@webilesoft.com"
                        faceClass="edvards-kazlouski"
                        phone="+375 29 254 4272"
                        name="Edvards Kazlouski"
                        socialNetworks={{
                            vk: 'https://vk.com/edvards_kazlouski',
                            fb: 'https://www.facebook.com/edvards.kazlouski',
                        }}
                        title="CEO"
                    />
                    <TeamMember
                        email="yury_kharytanovich@webilesoft.com"
                        faceClass="yury-kharytanovich"
                        phone="+375 29 618 3093"
                        name="Yury Kharytanovich"
                        socialNetworks={{
                            vk: 'https://vk.com/id28159438',
                        }}
                        title="Software Engineer"
                    />
                    <TeamMember
                        email="stanislav_belous@webilesoft.com"
                        faceClass="stanislav-belous"
                        phone="+375 29 218 1003"
                        name="Stanislav Belous"
                        socialNetworks={{
                            vk: 'https://vk.com/id68681704',
                        }}
                        title="Software Engineer"
                    />
                </ul>
            </div>
        );
    }
}
