import React from 'react';
import classNames from 'classnames';

import { Translater } from 'lang';

import './style';

// constants
import { SocialNetworks } from './constants';

// components
import AbstractComponent from 'components/abstract_component';

class TeamMember extends AbstractComponent {
    render () {
        const socialNetworks = [];

        Object.keys(SocialNetworks).forEach((key, index) => {
            const socialNetwork = this.props.socialNetworks[key];

            if (socialNetwork) {
                socialNetworks.push(
                    <a
                        key={`${key}_${index}`}
                        href={socialNetwork}
                    >
                        <span className={classNames('fa', SocialNetworks[key])} />
                    </a>
                );
            }
        });

        return (
            <div className="team-member">
                <div className={classNames('team-member-face', this.props.faceClass)} >
                    <span className="team-member-face-name">{this.props.name}</span>
                </div>
                <div className="team-member-back">
                    <span className="team-member-back-name" >{this.props.name}</span>
                    <span className="team-member-back-role" >{this.props.title}</span>
                    <a
                        href={`tel:${this.props.phone}}`}
                        className="team-member-back-phone"
                    >
                        {this.props.phone}
                    </a>
                    <a
                        className="team-member-back-email"
                        href={`mailto:${this.props.email}`}
                    >
                        <Translater label="KEY_OUR_TEAM_EMAIL_ME" />
                    </a>
                    <ul className="team-member-social">
                        {socialNetworks}
                    </ul>
                </div>
            </div>
        );
    }
}

TeamMember.propTypes = {
    faceClass: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    phone: React.PropTypes.string.isRequired,
    socialNetworks: React.PropTypes.any.isRequired, // eslint-disable-line react/forbid-prop-types
    title: React.PropTypes.string.isRequired,
    email: React.PropTypes.string,
};

export default TeamMember;
