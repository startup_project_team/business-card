import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

import { Translater } from 'lang';

export default class OurVacancies extends AbstractComponent {
    render () {
        return (
            <div className="our-vacancies">
                <Translater
                    className="our-vacancies-title"
                    label="KEY_VACANCIES_TITLE"
                />
                <div className="vacancies-container">
                    <div className="vacancies-designer mouse" />
                    <section className="vacancies-text-container">
                        <Translater
                            className="vacancies-description"
                            label="KEY_VACANCIES_DESCRIPTION_PART_1"
                        />
                        <Translater
                            className="vacancies-description"
                            label="KEY_VACANCIES_DESCRIPTION_PART_2"
                        />
                        <ul className="vacancies-list">
                            <li className="vacancy">
                                <i className="fa fa-circle" />
                                <Translater
                                    className="vacancy-text"
                                    label="KEY_VACANCY_CEO"
                                />
                            </li>
                            <li className="vacancy">
                                <i className="fa fa-circle" />
                                <Translater
                                    className="vacancy-text"
                                    label="KEY_VACANCY_WEB_DESIGNER"
                                />
                            </li>
                            <li className="vacancy">
                                <i className="fa fa-circle" />
                                <Translater
                                    className="vacancy-text"
                                    label="KEY_VACANCY_SALES"
                                />
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
        );
    }
}
