import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

// child components
import AboutUsInfo from './about_us_info';
import OurVacancies from './our_vacancies';
import OurTeam from './our_team';

export default class AboutUs extends AbstractComponent {
    // <OurReviews />
    render () {
        return (
            <div className="about-us-container">
                <AboutUsInfo />
                <OurVacancies />
                <OurTeam />
            </div>
        );
    }
}
