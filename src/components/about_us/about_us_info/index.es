import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

import { Translater } from 'lang';

export default class AboutUsInfo extends AbstractComponent {
    render () {
        return (
            <div className="about-us-info">
                <Translater
                    className="about-us-title"
                    label="KEY_ABOUT_US_TITLE"
                />
                <div className="about-us-info-container">
                    <div className="about-us-developer mouse" />
                    <section className="about-us-text-container">
                        <Translater
                            className="main-info"
                            label="KEY_ABOUT_US_MAIN_INFO"
                        />
                        <Translater
                            className="our-short-portfolio"
                            label="KEY_ABOUT_US_PORTFOLIO"
                        />
                    </section>
                </div>
                <Translater
                    className="became-client-garanties"
                    label="KEY_ABOUT_US_CLIENT_GARANTIES"
                />
                <div className="about-us-garanties-container">
                    <ul className="about-us-garanties">
                        <li className="client-guarantee">
                            <i className="fa fa-cog" />
                            <Translater
                                className="client-guarantee-text"
                                label="KEY_ABOUT_US_CONTRACT_GARANTEE"
                            />
                        </li>
                        <li className="client-guarantee">
                            <i className="fa fa-comments" />
                            <Translater
                                className="client-guarantee-text"
                                label="KEY_ABOUT_US_CONTACT_WITH_SLAVES"
                            />
                        </li>
                        <li className="client-guarantee">
                            <i className="fa fa-history" />
                            <Translater
                                className="client-guarantee-text"
                                label="KEY_ABOUT_US_REPORTS"
                            />
                        </li>
                        <li className="client-guarantee">
                            <i className="fa fa-usd" />
                            <Translater
                                className="client-guarantee-text"
                                label="KEY_ABOUT_US_DISCOUNT"
                            />
                        </li>
                    </ul>
                    <div className="about-us-customer mouse" />
                </div>
            </div>
        );
    }
}
