import React from 'react';
import classNames from 'classnames';

import AbstractComponent from 'components/abstract_component';
import { Translater } from 'lang';

import './style';

class ReviewMosaicItem extends AbstractComponent {
    render () {
        return (
            <div className="review-mosaic-item-container">
                <div className={classNames('review-picture', this.props.picture)} />
                <div className="review-picture-shadow" />
                <div className="review-mask" />
                <Translater
                    className="review-title"
                    label={this.props.title}
                />
            </div>
        );
    }
}

ReviewMosaicItem.propTypes = {
    picture: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
};

export default ReviewMosaicItem;
