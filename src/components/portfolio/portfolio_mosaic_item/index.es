import React from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import AbstractComponent from 'components/abstract_component';
import { Translater } from 'lang';

import './style';

class PortfolioMosaicItem extends AbstractComponent {
    render () {
        return (
            <div className="portfolio-mosaic-item-container">
                <div className={classNames('portfolio-picture', this.props.picture)} />
                <div className="portfolio-picture-shadow" />
                <div className="portfolio-text-container">
                    <Translater
                        className="portfolio-title"
                        label={this.props.title}
                    />
                    <Translater
                        className="portfolio-description"
                        label={this.props.description}
                    />
                    <a
                        className="portfolio-link"
                        href={this.props.link}
                    >
                        <Translater
                            className="portfolio-link-text"
                            label="KEY_PORTFOLIO_GO_TO_LINK"
                        />
                    </a>
                </div>
                <div className="portfolio-technologies-container">
                    <div className="portfolio-technologies">
                        <Translater
                            className="portfolio-technologies-title"
                            label="KEY_PORTFOLIO_TECHNOLOGIES"
                        />
                        <ul className="portfolio-technologies-list">
                            {_.map(this.props.technologies, (technology, index) =>
                                <Translater
                                    key={`${technology}_${index}`}
                                    className="portfolio-technology"
                                    label={technology}
                                />
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

PortfolioMosaicItem.propTypes = {
    description: React.PropTypes.string.isRequired,
    link: React.PropTypes.string.isRequired,
    picture: React.PropTypes.string.isRequired,
    technologies: React.PropTypes.array.isRequired,
    title: React.PropTypes.string.isRequired,
};

export default PortfolioMosaicItem;
