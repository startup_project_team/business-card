import React from 'react';
import './style';
import AbstractComponent from 'components/abstract_component';

import { Translater } from 'lang';

// mosaic
// import Mosaic from 'components/mosaic';

// // components
// import PortfolioMosaicItem from './portfolio_mosaic_item';
// import ReviewMosaicItem from './review_mosaic_item';

// // constants
// import PortfolioItems from 'constants/portfolio';
// import Reviews from 'constants/reviews';

export default class Portfolio extends AbstractComponent {
                // <Mosaic
                //     className="portfolio-mosaic"
                //     itemComponent={PortfolioMosaicItem}
                //     items={PortfolioItems}
                // />
                // <Mosaic
                //     className="reviews-mosaic"
                //     itemComponent={ReviewMosaicItem}
                //     items={Reviews}
                // />
    render () {
        return (
            <div className="portfolio-container">
                <Translater
                    className="portfolio-coming-soon-title"
                    label="KEY_PORTFOLIO_COMING_SOON"
                />
            </div>
        );
    }
}
