import { ANIMATION_STEP } from 'constants/scrolling';

import { scrollToBottom } from 'actions/scrolling';

export function onScrollDown () {
    scrollToBottom(ANIMATION_STEP);
}
