import React from 'react';

import { Translater } from 'lang';

import './style';

// common
import AbstractComponent from 'components/abstract_component';
import Button from 'components/button';

import * as Actions from './actions';

export default class Welcome extends AbstractComponent {
    render () {
        return (
            <div className="welcome-container">
                <div className="welcome-background" />
                <div className="welcome-background-shadow" >
                    <div className="contact-us-container">
                        <Translater
                            label="KEY_WELCOME_BIG_PROBLEM"
                            className="contact-us-line problem"
                        />
                        <Translater
                            label="KEY_WELCOME_GREAT_SOLUTION"
                            className="contact-us-line solution"
                        />
                        <Button
                            className="contact-us-button"
                            label="KEY_WELCOME_CONTACT_US"
                            onClick={Actions.onScrollDown}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
