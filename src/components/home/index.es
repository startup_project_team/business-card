import React from 'react';
import AbstractComponent from 'components/abstract_component';

import './style';

// child components
import Welcome from './welcome';
import Services from './services';
import MiniContactUs from 'components/mini_contact_us';

export default class Home extends AbstractComponent {
    render () {
        return (
            <div className="home-container">
                <Welcome />
                <Services />
                <MiniContactUs mainTitle="KEY_MINI_CONTACT_US_TITLE" />
            </div>
        );
    }
}
