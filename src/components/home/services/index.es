import React from 'react';

import AbstractComponent from 'components/abstract_component';

import ServicesList from 'components/services_list';

import { SERVICES } from './constants';

import './style';

export default class Services extends AbstractComponent {
    render () {
        return <div className="services-container">
            <ServicesList
                items={SERVICES}
            />
        </div>;
    }
}
