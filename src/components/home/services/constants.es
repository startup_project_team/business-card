import { Paths } from 'router/constants';

export const SERVICES = [
    {
        imageClass: 'website',
        title: 'KEY_SERVICES_WEB_APP_TITLE',
        label: 'KEY_SERVICES_WEB_APP_SHORT_TEXT',
        path: Paths.DEVELOPMENT,
        // path: Paths.WEB,
    }, {
        imageClass: 'mobile',
        title: 'KEY_SERVICES_MOBILE_APP_TITLE',
        label: 'KEY_SERVICES_MOBILE_APP_SHORT_TEXT',
        path: Paths.DEVELOPMENT,
        // path: Paths.MOBILE
    }, {
        imageClass: 'seo',
        // imageClass: 'advertising',
        title: 'KEY_SERVICES_SEO_TITLE',
        label: 'KEY_SERVICES_SEO_SHORT_TEXT',
        path: Paths.SEO,
        // path: Paths.ADVERTISING,
    }, {
        imageClass: 'machine-learning',
        title: 'KEY_SERVICES_MACHINE_LEARNING_TITLE',
        label: 'KEY_SERVICES_MACHINE_LEARNING_SHORT_TEXT',
        path: Paths.DEVELOPMENT,
        // path: Paths.MACHINE_LEARNING
    }, {
        imageClass: 'big-data',
        title: 'KEY_SERVICES_BIG_DATA_TITLE',
        label: 'KEY_SERVICES_BIG_DATA_SHORT_TEXT',
        path: Paths.DEVELOPMENT,
        // path: Paths.BIG_DATA
    }, {
        imageClass: 'cms',
        title: 'KEY_SERVICES_CMS_TITLE',
        label: 'KEY_SERVICES_CMS_SHORT_TEXT',
        path: Paths.DEVELOPMENT,
        // path: Paths.CMS
    },
];
