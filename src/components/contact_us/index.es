import React from 'react';
import AbstractComponent from 'components/abstract_component';

// components
import MiniContactUs from 'components/mini_contact_us';
import OurLocation from './our_location';

export default class ContactUs extends AbstractComponent {
    render () {
        return (
            <div className="contact-us">
                <OurLocation />
                <MiniContactUs
                    mainTitle="KEY_CONTACT_US_MAIN_TITLE"
                    textareaPlaceholder="KEY_CONTACT_US_PLACEHOLDER"
                />
            </div>
        );
    }
}
