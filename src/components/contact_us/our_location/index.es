import React from 'react';
import './style';

// components
import AbstractComponent from 'components/abstract_component';
import { Translater, translate } from 'lang';

// contants
import { DIRECTION } from './constants';

export default class OurLocation extends AbstractComponent {
    render () {
        return (
            <div className="our-location-container" >
                <label className="our-location-title" >
                    <i className="fa fa-map-marker" />
                    <Translater
                        className="our-location-title-text"
                        label="KEY_OUR_LOCATION_TITLE"
                    />
                </label>
                <div className="our-location">
                    <div className="our-location-image" />
                    <div className="our-location-description">
                        <Translater
                            className="our-location-name"
                            label="KEY_LOCATION_HEADQUARTERS"
                        />
                        <Translater
                            className="our-location-synopsis"
                            label="KEY_LOCATION_ADDRESS"
                        />
                        <div className="our-location-row">
                            <a
                                href={`tel:${translate('KEY_LOCATION_PHONE', this.context.currentLanguage)}`}
                                className="our-location-phone-container"
                            >
                                <i className="fa fa-phone-square" />
                                <Translater
                                    className="our-location-phone"
                                    label="KEY_LOCATION_PHONE"
                                />
                            </a>
                            <a
                                className="our-location-directions-link"
                                href={DIRECTION}
                            >
                                <Translater
                                    className="our-location-directions-text"
                                    label="KEY_OUR_LOCATION_DIRECTION"
                                />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
